import React, {Component} from 'react'
import AddTaskForm from "../../components/ToDoList/AddTaskForm/AddTaskForm"
import Task from "../../components/ToDoList/Task/Task"
import Spinner from "../../components/UI/Spinner/Spinner";
import {connect} from 'react-redux';
import {addTask, change, deleteTask, resetField, updateTasks} from "../../store/actions";

class ToDoList extends Component {
	componentDidMount() {
		console.log('mount');
		this.props.updateTasks();
	}
	
	render() {
		let tasks = (
			<div className="items">
				{Object.keys(this.props.tasks).map((id) => {
					return <Task
						key={id}
						text={this.props.tasks[id].text}
						delete={() => this.props.deleteTask(id)}
					/>
				})}
			</div>
		);
		if (this.props.loading) tasks = <Spinner/>;
		return (
			<div className="main">
				<div className="container">
					<AddTaskForm
						value={this.props.inputValue}
						add={() => this.props.addTask()}
						change={(event) => this.props.change(event)}
						click={this.props.resetField}
					/>
					{tasks}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		tasks: state.tasks,
		inputValue: state.inputValue,
		loading: state.loading
	}
};

const mapDispatchToProps = dispatch => {
	return {
		updateTasks: () => dispatch(updateTasks()),
		addTask: () => dispatch(addTask()),
		change: (event) => dispatch(change(event)),
		resetField: () => dispatch(resetField()),
		deleteTask: (id) => dispatch(deleteTask(id))
	}
};


export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
