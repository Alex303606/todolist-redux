import {DELETE, FETCH_TODO_SUCCESS, INPUT, RESET} from "./actions";

const initialState = {
	tasks: {},
	inputValue: 'Add new task',
	loading: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_TODO_SUCCESS:
			return {...state, tasks: action.tasks};
		case INPUT:
			return {...state, inputValue: action.value};
		case RESET:
			return {...state, inputValue: action.value};
		case DELETE:
			let tasks = {...state.tasks};
			delete tasks[action.id];
			return {...state, tasks: tasks};
		default:
			return state;
	}
};

export default reducer;