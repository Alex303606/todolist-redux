import axios from "axios/index";

export const FETCH_TODO_REQUEST = 'FETCH_TODO_REQUEST';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';
export const INPUT = 'INPUT';
export const RESET = 'RESET';
export const DELETE = 'DELETE';

export const fetchTodoRequest = () => {
	return {type: FETCH_TODO_REQUEST};
};

export const fetchTodoSuccess = (tasks) => {
	return {type: FETCH_TODO_SUCCESS, tasks};
};

export const fetchTodoError = () => {
	return {type: FETCH_TODO_ERROR};
};

export const updateTasks = () => {
	return dispatch => {
		dispatch(fetchTodoRequest());
		axios.get('/tasks.json').then(response => {
			if (response.data) dispatch(fetchTodoSuccess(response.data));
		}, error => {
			dispatch(fetchTodoError());
		});
	}
};

export const addTask = () => {
	return (dispatch, getState) => {
		let task = {text: getState().inputValue};
		axios.post('/tasks.json', task).then(() => dispatch(updateTasks()));
		dispatch({type: INPUT, value: 'Add new task'});
	}
};

export const change = (event) => {
	return (dispatch) => {
		dispatch({type: INPUT, value: event.target.value})
	}
};

export const resetField = () => {
	return (dispatch) => {
		dispatch({type: RESET, value: ''});
	}
};

export const deleteTask = (id) => {
	return (dispatch) => {
		axios.delete(`/tasks/${id}.json`);
		dispatch({type: DELETE, id: id});
	}
};
