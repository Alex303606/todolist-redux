import React from 'react';
import './Task.css';

const Task = props => {
	return (
		<div className="item">
			<span>{props.text}</span>
			<button onClick={props.delete}>
				<i className="fas fa-trash-alt" aria-hidden="true"/>
			</button>
		</div>
	)
};

export default Task;